from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^user$', views.user_dashboard, name="user-dashboard"),
    url(r'^add/resume$', views.resume_form, name="add-resume"),
    url(r'^edit/resume/(?P<slug>[-\w]+)$', views.edit_resume_form, name="edit-resume"),
    url(r'^education$', views.education, name="education"),
    url(r'^add/education$', views.add_education, name="add-education"),
    url(r'^edit/education/(?P<id>[0-9]+)$', views.edit_education, name="edit-education"),
    url(r'^experience$', views.experience, name="experience"),
    url(r'^add/experience$', views.add_experience, name="add-experience"),
    url(r'^edit/experience/(?P<id>[0-9]+)$', views.edit_experience, name="edit-experience"),
    url(r'^skill$', views.skill, name="skill"),
    url(r'^add/skill$', views.add_skill, name="add-skill"),
    url(r'^edit/skill/(?P<id>[0-9]+)$', views.edit_skill, name="edit-skill"),
    url(r'^reference$', views.reference, name="reference"),
    url(r'^add/reference$', views.add_reference, name="add-reference"),
    url(r'^edit/reference/(?P<id>[0-9]+)$', views.edit_reference, name="edit-reference"),
    url(r'^add/setting$', views.usersetting, name="usersetting"),
    url(r'^edit/setting/(?P<id>[0-9]+)$', views.edit_usersetting, name="edit-usersetting"),
]
