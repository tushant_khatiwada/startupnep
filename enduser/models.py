from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse

from products.utils import unique_slug_generator
from products.models import Category


class UserSetting(models.Model):
    user = models.OneToOneField(User)
    job_interest = models.ManyToManyField(Category, related_name='user')
    is_email_notification = models.BooleanField(
        default=True, help_text="Email me job recommendations based on my interests and preferences")

    class Meta:
        verbose_name = 'User Setting'
        verbose_name_plural = 'User Settings'

    def __str__(self):
        return self.user.username


# class Contact(models.Model):
#     user_from = models.ForeignKey(User, related_name='rel_from_set')
#     user_to = models.ForeignKey(User, related_name='rel_to_set')
#     created = models.DateTimeField(auto_now_add=True, db_index=True)

#     class Meta:
#         ordering = ('-created',)

#     def __str__(self):
#         return '{} follows {}'.format(self.user_from, self.user_to)


# # Add following field to User dynamically
# User.add_to_class('following',
#                   models.ManyToManyField('self',
#                                          through=Contact,
#                                          related_name='followers',
#                                          symmetrical=False))
