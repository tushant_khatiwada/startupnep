from django import forms

from resume.models import Resume, Experience, Education, Skill, Reference
from .models import UserSetting

class ResumeForm(forms.ModelForm):
    class Meta:
        model = Resume
        exclude=("applicant", "slug")

class ExperienceForm(forms.ModelForm):
    class Meta:
        model = Experience
        exclude=("resume",)

class EducationForm(forms.ModelForm):
    class Meta:
        model = Education
        exclude=("resume",)

class SkillForm(forms.ModelForm):
    class Meta:
        model = Skill
        exclude=("resume",)

class ReferenceForm(forms.ModelForm):
    class Meta:
        model = Reference
        exclude=("resume",)

class UserSettingForm(forms.ModelForm):
    class Meta:
        model = UserSetting
        exclude=("user", )
