from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from resume.models import Resume, Education, Experience, Skill, Reference
from .forms import ResumeForm, EducationForm, ExperienceForm, SkillForm, ReferenceForm, UserSettingForm
from .models import UserSetting


def addForm(request, form, template):
    if request.method == "POST":
        if form.is_valid():
            resume_instance = Resume.objects.get(applicant=request.user)
            new_form = form.save(commit=False)
            new_form.resume = resume_instance
            new_form.save()
            messages.success(request, 'Thank you')
        messages.warning(request, 'Correct the error')
    context = {
        'form': form
    }
    return render(request, template, context)


def edit_form(request, form, template):
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'successfully updated')
        messages.warning(request, 'Correct the error')
    context = {
        'form': form
    }
    return render(request, template, context)


def user_dashboard(request):
    if request.user.is_authenticated():
        context = {'user': request.user}
        return render(request, 'userDashboard/index.html', context)
    return render(request, 'company/home.html', context={})


def resume_form(request):
    template = 'userDashboard/user/resume.html'
    user = request.user
    try:
        resume = Resume.objects.get(applicant=request.user)
        if (user and resume):
            form = ResumeForm(request.POST or None, instance=resume)
            if request.method == "POST":
                if form.is_valid():
                    form.save()
                    messages.success(
                        request, 'Thank you for updating the personal detail')
                messages.warning(request, 'Error')
            context = {'form': form}
            return render(request, template, context)
    except Resume.DoesNotExist:
        print('error')
    form = ResumeForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        peronal_detail = form.save(commit=False)
        personal_detail.applicant = user
        personal_detail.save()
        messages.success(request, 'Thank you for updating the personal detail')
    messages.warning(request, 'Error is here')
    context = {'form': form}
    return render(request, template, context)


def edit_resume_form(request, slug=None):
    resume = get_object_or_404(Resume, slug=slug)
    form = ResumeForm(request.POST or None, instance=resume)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'Thank you for updating resume')
            return redirect('/')
    context = {
        'form': form
    }
    return render(request, 'userDashboard/user/resume.html', context)


def education(request):
    context = {}
    try:
        #education_instance = Resume.objects.get(applicant=request.user).educations.all()
        # below is the optimized way
        education_instance = Education.objects.filter(
            resume__applicant=request.user).prefetch_related('resume')
        context['education'] = education_instance
    except Resume.DoesNotExist:
        context['education'] = ''
    return render(request, 'userDashboard/user/education.html', context)


def add_education(request):
    form = addForm(request, EducationForm(request.POST or None),
                   'userDashboard/user/add_education.html')
    return form


def edit_education(request, id):
    instance = get_object_or_404(Education, id=id)
    form = edit_form(request, EducationForm(request.POST or None,
                                            instance=instance), 'userDashboard/user/add_education.html')
    return form


def experience(request):
    context = {}
    try:
        experience_instance = Experience.objects.filter(
            resume__applicant=request.user).prefetch_related('resume')
        context['experiences'] = experience_instance
    except Resume.DoesNotExist:
        context['experiences'] = ''
    return render(request, 'userDashboard/user/experience.html', context)


def add_experience(request):
    form = addForm(request, ExperienceForm(request.POST or None),
                   'userDashboard/user/add_experience.html')
    return form


def edit_experience(request, id):
    instance = get_object_or_404(Experience, id=id)
    form = edit_form(request, ExperienceForm(request.POST or None,
                                             instance=instance), 'userDashboard/user/add_experience.html')
    return form


def skill(request):
    context = {}
    try:
        skill_instance = Skill.objects.filter(
            resume__applicant=request.user).prefetch_related('resume')
        context['skills'] = skill_instance
    except Resume.DoesNotExist:
        context['skills'] = ''
    return render(request, 'userDashboard/user/skill.html', context)


def add_skill(request):
    form = addForm(request, SkillForm(request.POST or None),
                   'userDashboard/user/add_skill.html')
    return form


def edit_skill(request, id):
    instance = get_object_or_404(Skill, id=id)
    form = edit_form(request, SkillForm(request.POST or None,
                                        instance=instance), 'userDashboard/user/add_skill.html')
    return form


def reference(request):
    context = {}
    try:
        reference_instance = Reference.objects.filter(
            resume__applicant=request.user).prefetch_related('resume')
        context['references'] = reference_instance
    except Resume.DoesNotExist:
        context['references'] = ''
    return render(request, 'userDashboard/user/reference.html', context)


def add_reference(request):
    form = addForm(request, ReferenceForm(request.POST or None),
                   'userDashboard/user/add_reference.html')
    return form


def edit_reference(request, id):
    instance = get_object_or_404(Reference, id=id)
    form = edit_form(request, ReferenceForm(request.POST or None,
                                            instance=instance), 'userDashboard/user/add_reference.html')
    return form


def usersetting(request):
    form = UserSettingForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            user_setting = form.save(commit=False)
            user_setting.user = request.user
            user_setting.save()
            messages.success(request, 'Thank you')
        messages.warning(request, 'correct the error')
    context = {
        'form': form
    }
    return render(request, 'userDashboard/user/user_setting.html', context)


def edit_usersetting(request, id):
    print('id', id)
    user_setting_instance = get_object_or_404(UserSetting, id=id)
    form = UserSettingForm(request.POST or None,
                           instance=user_setting_instance)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'Thank you')
        messages.warning(request, 'correct the error')
    context = {
        'form': form
    }
    return render(request, 'userDashboard/user/user_setting.html', context)


# def reference_form(request):
#     form = ResumeForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             resume = form.save(commit=False)
#             resume.applicant = request.user
#             resume.save()
#             message.success(request, 'Thank you for creating resume. You can now apply for the job')
#             return redirect('/')
#     context = {
#         'form': form
#     }
#     return render(request, 'userDashboard/user/resume.html', context)
#
# def experience_form(request):
#     form = ResumeForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             resume = form.save(commit=False)
#             resume.applicant = request.user
#             resume.save()
#             message.success(request, 'Thank you for creating resume. You can now apply for the job')
#             return redirect('/')
#     context = {
#         'form': form
#     }
#     return render(request, 'userDashboard/user/resume.html', context)
#
#
# def skill_form(request):
#     form = ResumeForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             resume = form.save(commit=False)
#             resume.applicant = request.user
#             resume.save()
#             message.success(request, 'Thank you for creating resume. You can now apply for the job')
#             return redirect('/')
#     context = {
#         'form': form
#     }
#     return render(request, 'userDashboard/user/resume.html', context)
#
#
#
# def reference_form(request):
#     form = ResumeForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             resume = form.save(commit=False)
#             resume.applicant = request.user
#             resume.save()
#             message.success(request, 'Thank you for creating resume. You can now apply for the job')
#             return redirect('/')
#     context = {
#         'form': form
#     }
#     return render(request, 'userDashboard/user/resume.html', context)
