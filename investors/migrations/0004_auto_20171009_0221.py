# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-09 02:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('investors', '0003_auto_20171009_0221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='investor',
            name='followers',
            field=models.ManyToManyField(blank=True, related_name='followers', to=settings.AUTH_USER_MODEL),
        ),
    ]
