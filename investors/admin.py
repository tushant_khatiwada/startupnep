from django.contrib import admin
from django.db import models

from .models import Investor, Investment


class InvestmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = Investment


class InvestorAdmin(admin.ModelAdmin):
    list_display = ('name', 'investor', 'contact_email')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = Investor

admin.site.register(Investor, InvestorAdmin)
admin.site.register(Investment, InvestmentAdmin)
