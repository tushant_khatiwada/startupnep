from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView
from django.contrib.auth.models import User

from .models import Investor, Investment


class Investors(ListView):

    def get_queryset(self):
        return Investor.objects.all()


class InvestorDetail(DetailView):

    def get_queryset(self):
        return Investors.objects.get(slug=self.slug)


def follow_investor(request, slug=None):
    user = request.user
    investor = Investor.objects.get(slug=slug)
    investor.followers.add(user)
    investor.save()
    return redirect('/')


def follow_unfollow_investor(request, action, slug=None):
    follower = request.user
    investor = Investor.objects.get(slug=slug)
    if action == "add":
        Investor.follow_investor(investor, follower)
    elif action == "remove":
        Investor.unfollow_investor(investor, follower)
    return redirect('/')
