from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^investors$', views.Investors.as_view(), name="list-investors"),
    url(r'^investor/(?P<slug>[-\w]+)$',
        views.follow_investor, name="follow-investor"),
    url(r'^investor/(?P<action>.+)/(?P<slug>[-\w]+)$',
        views.follow_unfollow_investor, name="follow-unfollow-investor"),
]
