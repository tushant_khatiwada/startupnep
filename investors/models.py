from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse

from products.utils import unique_slug_generator


STATUS = (
    ('approved', 'Approved'),
    ('pending', 'Pending'),
    ('rejected', 'Rejected'),
)


def upload_company_image_path(instance, filename):
    return 'investments/companies/{0}'.format(instance.name)


class Investment(models.Model):
    name = models.CharField(max_length=200, blank=True,
                            null=True, help_text='Name of the company')
    slug = models.SlugField(max_length=200, unique=True)
    company_logo = models.ImageField(
        null=True, blank=True, upload_to=upload_company_image_path)

    class Meta:
        verbose_name = 'Investment'
        verbose_name_plural = 'Investments'

    def __str__(self):
        return self.name


class Investor(models.Model):
    investor = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, blank=False,
                            null=False, help_text='Full Name')
    slug = models.SlugField(max_length=200, unique=True)
    contact_email = models.EmailField(null=True, blank=True)
    position = models.CharField(max_length=200, blank=True, null=True)
    investment = models.ForeignKey(
        Investment, related_name='investor', blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    # market_location = models.CharField(max_length=100, blank=True, null=True)
    followers = models.ManyToManyField(
        User, related_name='followers', blank=True)
    website = models.URLField(null=True, blank=True)
    twitter_url = models.URLField(null=True, blank=True)
    linkedin_url = models.URLField(null=True, blank=True)
    status = models.CharField(choices=STATUS, default="pending", max_length=8)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Investor'
        verbose_name_plural = 'Investors'

    def __str__(self):
        return self.investor.username

    # def get_absolute_url(self):
    # return reverse('investors:investor-detail', kwargs={'slug': self.slug})

    def get_follow_url(self):
        return reverse('investors:follow-investor', kwargs={'slug': self.slug})

    @classmethod
    def follow_investor(cls, investor, follower):
        investor, created = cls.objects.get_or_create(
            investor=investor.investor)
        investor.followers.add(follower)

    @classmethod
    def unfollow_investor(cls, investor, follower):
        investor, created = cls.objects.get_or_create(
            investor=investor.investor)
        investor.followers.remove(follower)


def pre_save_slug(sender, instance, *args, **kwargs):
    if instance.slug == '':
        instance.slug = unique_slug_generator(instance)
pre_save.connect(pre_save_slug, sender=Investor)
pre_save.connect(pre_save_slug, sender=Investment)
