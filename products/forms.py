from django import forms
from django.contrib.auth.models import User

from .models import Product, Company, Achievement, Founder, Profile


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ('name', 'description', 'logo', 'city', 'employees', 'email', 'android_app',
                  'ios_app', 'twitter', 'facebook', 'linkedin', 'banner', 'established_year', 'is_hiring', 'hiring_url')
        widgets = {
            'established_year': forms.TextInput(attrs={'class': 'datepicker'}),
            'is_hiring': forms.CheckboxInput(attrs={'class': 'styled-checkbox'}),
            'logo': forms.FileInput(attrs={'class': 'inputfile'}),
            'banner': forms.FileInput(attrs={'class': 'inputfile'}),
        }


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = ('name', 'description', 'company',
                  'product_type', 'categories',)


class AchievementForm(forms.ModelForm):

    class Meta:
        model = Achievement
        fields = '__all__'


class FounderForm(forms.ModelForm):

    class Meta:
        model = Founder
        fields = '__all__'


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        exclude = ('user', )
