from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Count
# from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render

from .models import (Product, Company, Category, Achievement, Founder, Profile)
from .forms import (CompanyForm, ProductForm,
                    AchievementForm, FounderForm, UserForm, ProfileForm)

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions


def home(request):
    categories = Category.objects.annotate(
        product_count=Count('products')).order_by('-product_count')[:4]
    companies = Company.objects.all()[:12]
    context = {
        'categories': categories,
        'companies': companies
    }
    return render(request, 'company/home.html', context)


def update_profile(request):
    profile = Profile.objects.get_or_create(user=request.user)
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(
            request.POST or None, instance=profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(
                request, ('your profile was successfully updated!'))
            return redirect('products:profile')
        else:
            messages.error(
                request, ('There was an error updating your profile'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'CompanyDashboard/company/profile.html', {'user_form': user_form, 'profile_form': profile_form})


def dashboard(request):
    if request.user.is_authenticated:
        context = {
            'welcome': 'Welcome to StartupNEP'
        }
        return render(request, 'CompanyDashboard/index.html', context)
    categories = Category.objects.all()
    companies = Company.objects.all()[:12]
    context = {
        'categories': categories,
        'companies': companies
    }
    return render(request, 'company/home.html', context)


def list_company(request):
    companies = Company.objects.all()
    if (request.GET.get('category') is not None):
        print('true its not None')
        url_slug = request.GET.get('category')
        companies = Company.objects.filter(products__categories__slug=url_slug)
    context = {
        'companies': companies
    }
    return render(request, 'company/list_company.html', context)


def list_my_company(request):
    companies = Company.objects.filter(editor=request.user)
    page = request.GET.get('page', 1)
    paginator = Paginator(companies, 2)
    try:
        companies = paginator.page(page)
    except PageNotAnInteger:
        companies = paginator.page(1)
    except EmptyPage:
        companies = paginator.page(paginator.num_pages)
    # if (request.GET.get('category') is not None):
    #     print('true its not None')
    #     url_slug = request.GET.get('category')
    #     companies = companies.filter(products__categories__slug=url_slug)
    context = {
        'companies': companies
    }
    return render(request, 'CompanyDashboard/company/list_my_company.html', context)


def list_my_product(request):
    products = Product.objects.filter(creator=request.user)
    page = request.GET.get('page', 1)
    paginator = Paginator(products, 2)
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    context = {
        'products': products
    }
    return render(request, 'CompanyDashboard/company/list_my_product.html', context)


def list_my_achievement(request):
    achievements = Achievement.objects.filter(
        company_achievement__editor=request.user)
    page = request.GET.get('page', 1)
    paginator = Paginator(achievements, 2)
    try:
        achievements = paginator.page(page)
    except PageNotAnInteger:
        achievements = paginator.page(1)
    except EmptyPage:
        achievements = paginator.page(paginator.num_pages)
    context = {
        'achievements': achievements
    }
    return render(request, 'CompanyDashboard/company/list_my_achievement.html', context)


def view_company(request, slug):
    company_instance = get_object_or_404(Company, slug=slug)
    product_instance = Product.objects.filter(company=company_instance)
    context = {
        'company': company_instance,
        'products': product_instance
    }
    return render(request, 'company/view_company.html', context)


def post_company(request):
    print(request.path.lstrip('/'))
    company_instance = CompanyForm(request.POST or None, request.FILES or None)
    if request.method == 'POST' or request.method == 'FILES':
        if company_instance.is_valid():
            new_company = company_instance.save(commit=False)
            new_company.editor = request.user
            new_company.save()
            return redirect('products:post-product')
        else:
            company_instance = CompanyForm()
    return render(request, 'CompanyDashboard/company/add_company.html', {'form':
                                                                         company_instance})
    # return render(request, 'company/post_company.html', {'form':
    # company_instance})


def edit_company(request, slug=None):
    company_instance = get_object_or_404(Company, slug=slug)
    form = CompanyForm(request.POST or None,
                       request.FILES or None, instance=company_instance)
    if request.method == 'POST' or request.method == 'FILES':
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            form = CompanyForm()
    return render(request, 'CompanyDashboard/company/add_company.html', {'form': form})


def view_product(request, slug):
    product_instance = get_object_or_404(Product, slug=slug)
    categories = product_instance.categories.all()
    context = {
        'product': product_instance,
        'categories': categories
    }
    return render(request, 'company/view_product.html', context)


def post_product(request):
    product_instance = ProductForm(request.POST or None)
    if request.method == 'POST':
        if product_instance.is_valid():
            new_product = product_instance.save(commit=False)
            new_product.creator = request.user
            new_product.save()
            product_instance.save_m2m()
            return redirect('products:post-achievement')
        else:
            product_instance = ProductForm()
    return render(request, 'CompanyDashboard/company/add_product.html', {'form': product_instance})


def edit_product(request, slug=None):
    product_instance = get_object_or_404(Product, slug=slug)
    form = ProductForm(request.POST or None,
                       request.FILES or None, instance=product_instance)
    if request.method == 'POST' or request.method == 'FILES':
        if form.is_valid():
            form.save(commit=False)
            form.save()
            form.save_m2m()
            return redirect('products:post-product')
        else:
            form = ProductForm()
    return render(request, 'CompanyDashboard/company/add_product.html', {'form': form})


def post_achievement(request):
    achievement_form = AchievementForm(request.POST or None)
    if request.method == 'POST':
        if achievement_form.is_valid():
            achievement_form.save()
            return redirect('products:post-founder')
    return render(request, 'CompanyDashboard/company/add_achievement.html', {'form': achievement_form})


def edit_achievement(request, slug=None):
    achievement_instance = get_object_or_404(Achievement, slug=slug)
    achievement_form = AchievementForm(
        request.POST or None, instance=achievement_instance)
    if request.method == 'POST':
        if achievement_form.is_valid():
            achievement_form.save()
            AchievementForm()
    return render(request, 'CompanyDashboard/company/add_achievement.html', {'form': achievement_form})


def post_founder(request):
    founder_form = FounderForm(request.POST or None)
    if request.method == 'POST':
        if founder_form.is_valid():
            founder_form.save()
            FounderForm()
    return render(request, 'CompanyDashboard/company/add_founder.html', {'form': founder_form})


def edit_founder(request, slug=None):
    founder_instance = get_object_or_404(Founder, slug=slug)
    founder_form = FounderForm(
        request.POST or None, instance=founder_instance)
    if request.method == 'POST':
        if founder_form.is_valid():
            founder_form.save()
            FounderForm()
    return render(request, 'CompanyDashboard/company/add_achievement.html', {'form': founder_form})


def blog(request):
    context = {
        'coming_soon': 'Blog is Coming Soon. Sorry to make you wait'
    }
    return render(request, 'CompanyDashboard/company/blog.html', context)


class CompanyLikesAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, slug=None, format=None):
        company_id = request.data.get('id')
        print('company_id', company_id)
        action = request.data.get('action')
        if company_id and action:
            try:
                company = Company.objects.get(id=company_id)
                if action == "like":
                    company.likes.add(request.user)
                else:
                    company.likes.remove(request.user)
                return Response({'status': 'ok'})
            except:
                pass
        return Response({'status': 'ko'})

    # def get(self, request, slug=None, format=None):
    #     company_instance = get_object_or_404(Company, slug=slug)
    #     company_like = company_instance.likes.all()
    #     user = request.user
    #     unlike = False
    #     liked = False
    #     if user.is_authenticated():
    #         if user in company_like:
    #             company_instance.likes.remove(user)
    #             unlike = True
    #             liked = False
    #         else:
    #             company_instance.likes.add(user)
    #             liked = True
    #     data = {
    #         'unlike': unlike,
    #         'liked': liked
    #     }
    #     return Response(data)


# def company_like(request):
#     company_id = request.POST.get('id')
#     action = request.POST.get('action')
#     if company_id and action:
#         try:
#             company = Company.objects.get(id=company_id)
#             print('company', company)
#             if action == 'like':
#                 company.likes.add(request.user)
#             else:
#                 company.likes.remove(request.user)
#             return JsonResponse({'status': 'ok'})
#         except:
#             pass
#     return JsonResponse({'status': 'ko'})
