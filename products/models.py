from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse

from .utils import unique_slug_generator

STATUS = (
    ('approved', 'Approved'),
    ('pending', 'Pending'),
    ('rejected', 'Rejected'),
)

GENDER = (
    ('Male', 'Male'),
    ('Female', 'Female'),
    ('Other', 'Other'),
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=600, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    gender = models.CharField(
        choices=GENDER, max_length=6, blank=True, null=True)
    favourite_quote = models.CharField(max_length=400, blank=True, null=True)
    your_quote = models.CharField(max_length=400, blank=False, null=False)
    your_belief = models.CharField(max_length=600, blank=False, null=False)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
post_save.connect(create_user_profile, sender=User)


def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
post_save.connect(save_user_profile, sender=User)


def get_sentinel_user():
    return User.objects.filter(is_superuser=True).first()


class Type(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, unique=True)

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = 'Types'

    def __str__(self):
        return self.name


def upload_founder_image_path(instance, filename):
    return 'companies/founders/{0}'.format(instance.company.name)


class Founder(models.Model):
    company = models.ForeignKey('Company', blank=True, null=True)
    name = models.CharField(max_length=100)
    avatar = models.ImageField(
        null=True, blank=True, upload_to=upload_founder_image_path)

    class Meta:
        verbose_name = 'Founder'
        verbose_name_plural = 'Founders'

    def __str__(self):
        return 'Founder of {0} -> {1}'.format(self.company.name, self.name)


class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, unique=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    # @staticmethod
    # def top_category(self):
    #     return models.Count('categories').order_by('-categories_count')


class Product(models.Model):
    name = models.CharField(max_length=200, unique=True,
                            blank=False, null=False)
    description = models.CharField(max_length=400)
    company = models.ForeignKey(
        'Company', related_name='products', blank=True, null=True, on_delete=models.SET_NULL)
    website = models.URLField(unique=True, blank=True, null=True)
    slug = models.SlugField(unique=True)
    product_type = models.ForeignKey(Type, related_name='products')
    categories = models.ManyToManyField(Category, related_name='products')
    hits = models.PositiveIntegerField(default=0)
    creator = models.ForeignKey(
        User, related_name='products',  on_delete=models.SET(get_sentinel_user))
    status = models.CharField(max_length=8, choices=STATUS, default='pen')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.name

    # def top_products(self):
    #     count_category = Category.top_category()
    #     product = Product.objects.values('id').annotate(
    #         count_category)
    #     return product


class Company(models.Model):
    name = models.CharField(max_length=200, unique=True,
                            blank=False, null=False)
    slug = models.SlugField(unique=True)
    description = models.CharField(max_length=400)
    storyline = models.TextField(blank=True, null=True)
    editor = models.ForeignKey(User, related_name='company')
    logo = models.ImageField(null=True, blank=True, upload_to='companies/logo')
    banner = models.ImageField(
        null=True, blank=True, upload_to='companies/banner')
    city = models.CharField(null=True, blank=True, max_length=255)
    employees = models.PositiveIntegerField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    likes = models.ManyToManyField(
        User, blank=True, related_name='company_likes')
    android_app = models.URLField(null=True, blank=True)
    ios_app = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    linkedin = models.URLField(null=True, blank=True)
    established_year = models.PositiveIntegerField(null=True, blank=True)
    status = models.CharField(max_length=8, choices=STATUS, default='pen')
    is_hiring = models.BooleanField(default=False)
    hiring_url = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = ('Company')
        verbose_name_plural = ('Companies')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("products:view-company", kwargs={"slug": self.slug})

    def get_like_url(self):
        return reverse("products:like-company", kwargs={"slug": self.slug})


class Achievement(models.Model):
    company_achievement = models.ForeignKey(
        Company, related_name="achievement", on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = ("Achievement")
        verbose_name_plural = ("Achievements")


def pre_save_slug(sender, instance, *args, **kwargs):
    if instance.slug == '':
        instance.slug = unique_slug_generator(instance)
pre_save.connect(pre_save_slug, sender=Company)
pre_save.connect(pre_save_slug, sender=Product)
pre_save.connect(pre_save_slug, sender=Category)
