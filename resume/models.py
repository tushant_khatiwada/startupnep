from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse

from products.utils import unique_slug_generator

class Resume(models.Model):
    applicant = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=False, null=False, help_text="Full Name")
    slug = models.SlugField(max_length=50, unique=True)
    designation = models.CharField(max_length=200, blank=True, null=True)
    career_goal=models.CharField(max_length=500, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    email=models.EmailField(blank=False, null=False)
    website=models.URLField(blank=True, null=True)
    linkedin_url=models.URLField(blank=True, null=True)
    twitter_url=models.URLField(blank=True, null=True)
    facebook_url=models.URLField(blank=True, null=True)
    summary=models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        verbose_name = 'Resume'
        verbose_name_plural = 'Resume'

    def __str__(self):
        return self.name

class Education(models.Model):
    resume = models.ForeignKey(Resume, related_name='educations')
    name = models.CharField(max_length=100, blank=False, null=False, help_text="Name of an institution")
    course = models.CharField(max_length=200, blank=False, null=False, help_text="Name of a course")
    description = models.CharField(max_length=400, blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField()

    class Meta:
        verbose_name = 'Education'
        verbose_name_plural = 'Education'

    def __str__(self):
        return '{0} -> {1}'.format(self.resume.name, self.name)

    def edit_education_url(self):
        return reverse('enduser:edit-education', kwargs={'id': self.id})


class Experience(models.Model):
    resume = models.ForeignKey(Resume, related_name='experiences')
    designation = models.CharField(max_length=100, blank=True, null=True)
    company = models.CharField(max_length=100, blank=True, null=True)
    description=models.CharField(max_length=400, blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField()

    class Meta:
        verbose_name='Experience'
        verbose_name_plural='Experiences'

    def __str__(self):
        return self.designation

    def edit_experience_url(self):
        return reverse('enduser:edit-experience', kwargs={'id': self.id})

class Skill(models.Model):
    resume=models.ForeignKey(Resume, related_name="skills")
    name = models.CharField(max_length=100, blank=True, null=True, help_text="Name of the skill")

    class Meta:
        verbose_name='Skill'
        verbose_name_plural='Skills'

    def __str__(self):
        return self.name

    def edit_skill_url(self):
        return reverse('enduser:edit-skill', kwargs={'id': self.id})


class Reference(models.Model):
    resume = models.ForeignKey(Resume, related_name='references')
    name_of_refree=models.CharField(max_length=100, blank=False, null=False)
    designation_of_refree = models.CharField(max_length=100, blank=False, null=False)
    phone_number=models.CharField(max_length=50, blank=False, null=False)

    class Meta:
        verbose_name='Reference'
        verbose_name_plural='References'

    def __str__(self):
        return self.name_of_refree

    def edit_reference_url(self):
        return reverse('enduser:edit-reference', kwargs={'id': self.id})
