from django.contrib import admin

from .models import Resume, Experience, Education, Skill, Reference

class ExperienceAdmin(admin.StackedInline):
    model = Experience
    extra=1

class EducationAdmin(admin.StackedInline):
    model = Education
    extra=1

class SkillAdmin(admin.StackedInline):
    model = Skill
    extra=1

class ReferenceAdmin(admin.StackedInline):
    model = Reference
    extra=1

class ResumeAdmin(admin.ModelAdmin):
    list_display = ('name', 'applicant', 'designation', 'email')
    prepopulated_fields = {'slug': ('name',)}
    inlines=[ExperienceAdmin, EducationAdmin, SkillAdmin, ReferenceAdmin]
    class Meta:
        model = Resume
admin.site.register(Resume, ResumeAdmin)
admin.site.register(Experience)
admin.site.register(Education)
admin.site.register(Skill)
admin.site.register(Reference)
