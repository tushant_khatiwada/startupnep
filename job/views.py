from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib import messages

from django.core.paginator import Paginator

# Create your views here.
from .models import Job, Category, Position, WorkSchedule
from .tasks import send_resume_to_job_poster
from .forms import JobForm


def list_job(request):
    jobs = Job.objects.all()
    context = {
        'jobs': jobs
    }
    return render(request, 'company/list_job.html', context)


def detail_job(request, slug):
    job = get_object_or_404(Job, slug=slug)
    context = {
        'job': job
    }
    return render(request, 'company/job_detail.html', context)


def post_job(request):
    job_form = JobForm(request.POST or None)
    if request.method == 'POST':
        if job_form.is_valid():
            job_form.save(commit=False)
            # I may need to process something so for now I am leaving this as
            # it is
            job_form.save()
            job_form.save_m2m()
            messages.success(request, 'Your job has been successfully posted!')
            return redirect('/')
        messages.warning(request, 'Please correct the error below.')
    context = {'form': job_form}
    return render(request, 'company/post_job.html', context)


def edit_job(request, slug=None):
    job_instance = get_object_or_404(Job, slug=slug)
    job_form = JobForm(request.POST or None, instance=job_instance)
    if request.method == 'POST':
        if job_form.is_valid():
            job_form.save()
            job_form.save_m2m()
            messages.success(
                request, 'Your job has been successfully updated!')
            return redirect('/')
        messages.warning(request, 'Please correct the error below.')
    context = {'form': job_form}
    return render(request, 'company/post_job.html', context)


def apply_job(request, slug=None):
    job_instance = get_object_or_404(Job, slug=slug)
    email = request.user.email
    send_resume_to_job_poster.delay(job_instance.name, email)
    return redirect('jobs:list-job')
