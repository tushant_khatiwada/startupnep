from django.contrib import admin
from django.db import models
from markdownx.admin import MarkdownxModelAdmin

from markdownx.widgets import AdminMarkdownxWidget

from .models import Job, Category, Position, WorkSchedule


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = Category


class PositionAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = Position


class WorkScheduleAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = WorkSchedule


class JobAdmin(admin.ModelAdmin):
    list_display = ('name', 'company', 'category', 'estimated_salary')
    prepopulated_fields = {'slug': ('name',)}
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }

    class Meta:
        model = Job

admin.site.register(Job, JobAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Position, PositionAdmin)
admin.site.register(WorkSchedule, WorkScheduleAdmin)
