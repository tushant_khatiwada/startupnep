from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string

from weasyprint import HTML
from io import BytesIO
from django.conf import settings
from django.core.mail import send_mail, EmailMessage

from celery import shared_task

#from .models import Job

def html_to_pdf_view(resume):
    html_string = render_to_string('resume/resume.html', {'resume': resume})

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/resume_{}.pdf'.format(resume.id))

    fs = FileSystemStorage('/tmp')
    with fs.open('/tmp/resume_{}.pdf'.format(resume.id)) as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response[
            'Content-Disposition'] = 'attachment; filename="resume_{}.pdf"'.format(resume.id)
        return response

    return response


@shared_task
def post_newly_created_job_to_facebook(job):
    print('will post')


@shared_task
def send_resume_to_job_poster(poster, seeker):
    resume = User.objects.get(email=seeker).resume
    generated_pdf = html_to_pdf_view(resume)
    subject = "{} has applied for the job".format(resume.name)
    from_email = settings.EMAIL_HOST_USER
    to_list = [seeker]
    message = "if you don't want to recieve such message please we can't do anything for you."
    email = EmailMessage(
        subject,
        message,
        from_email,
        to_list
    )
    email.attach_file('/tmp/resume_{}.pdf'.format(resume.id))
    # email.attach("resume_{}.pdf".format(resume.id),
    #              generated_pdf, 'application/pdf')
    try:
        return email.send()
    except Exception as e:
        print('exeption is', e)


# send_mail('subject', 'message', 'sender email', [
#           'receipient email'],    fail_silently=False)


@shared_task
def send_notification_to_user(job_id, company_name, job_title, category):
    email_list=[]
    users = User.objects.filter(usersetting__is_email_notification=True).filter(usersetting__job_interest__name=category)
    for user in users:
        print ('user', user)
        email_list.append(user.email)
    print('email_list', email_list)
    try:
        subject="The following job has been added recently"
        message="You may want to checkin the job that match your interests"
        from_email=settings.EMAIL_HOST_USER
        to_list=email_list
        return send_mail(subject, message, from_email, to_list, fail_silently=False)
    except Exception as e:
        print ('exception as', e)
