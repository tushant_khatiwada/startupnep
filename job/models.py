from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from markdownx.models import MarkdownxField
from django.core.urlresolvers import reverse

from products.utils import unique_slug_generator
from products.models import Company
from .tasks import send_notification_to_user

STATUS = (
    ('approved', 'Approved'),
    ('pending', 'Pending'),
    ('rejected', 'Rejected'),
)


# WORK_SCHEDULE = (
#     ('FT', 'Full Time'),
#     ('PT', 'Part Time'),
# )

# POSITION = (
# 		('CO', 'Contractor'),
# 		('EM', 'Employee'),
# 		('IN', 'Intern'),
# 		('TE', 'Temporary'),
# 		('FR', 'Freelance'),
# )


class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=50, unique=True)
    description = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class WorkSchedule(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    slug = models.SlugField(max_length=50, unique=True)

    class Meta:
        verbose_name = 'Work Schedule'
        verbose_name_plural = 'Work Schedules'

    def __str__(self):
        return self.name


class Position(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    slug = models.SlugField(max_length=50, unique=True)

    class Meta:
        verbose_name = 'Position'
        verbose_name_plural = 'Positions'

    def __str__(self):
        return self.name


class Job(models.Model):
    company = models.ForeignKey(
        Company, related_name='jobs', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, blank=True,
                            null=True, help_text='Title for the Job')
    slug = models.SlugField(max_length=200, unique=True)
    category = models.ForeignKey(Category, related_name='jobs')
    # description = models.TextField(blank=False, null=False)
    description = MarkdownxField()
    city = models.CharField(max_length=100, blank=False, null=False)
    address = models.CharField(max_length=100, blank=False, null=False)
    how_to_apply = models.CharField(max_length=300, blank=True, null=True)
    position = models.ManyToManyField(
        Position)
    is_telecommute = models.BooleanField(
        default=False, blank=True, help_text='Is this job available for remote workers?')
    work_schedule = models.ManyToManyField(
        WorkSchedule)
    estimated_salary = models.PositiveIntegerField(default='1000')
    website = models.URLField(null=True, blank=True)
    job_url = models.URLField(null=True, blank=True)
    company_email = models.EmailField(null=True, blank=True)
    job_email = models.EmailField(null=True, blank=True)
    status = models.CharField(choices=STATUS, default="pending", max_length=8)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Job'
        verbose_name_plural = 'Jobs'

    def __str__(self):
        return '{0} -> {1}'.format(self.company.name, self.name)

    def get_absolute_url(self):
        return reverse('jobs:job-detail', kwargs={'slug': self.slug})

    def get_edit_url(self):
        return reverse('jobs:edit-job', kwargs={'slug': self.slug})

    def get_job_apply_url(self):
        return reverse('jobs:apply-job', kwargs={'slug': self.slug})


def pre_save_slug(sender, instance, *args, **kwargs):
    if instance.slug == '':
        instance.slug = unique_slug_generator(instance)
pre_save.connect(pre_save_slug, sender=Job)
pre_save.connect(pre_save_slug, sender=Position)
pre_save.connect(pre_save_slug, sender=WorkSchedule)
pre_save.connect(pre_save_slug, sender=Category)


def when_job_is_verified_send_email_to_listening_user(sender, instance, **kwargs):
    if instance.status == 'approved':
        send_notification_to_user.delay(instance.id, instance.company.name, instance.name, instance.category.name)

post_save.connect(when_job_is_verified_send_email_to_listening_user, sender=Job)
