from django import forms

from .models import Job


class JobForm(forms.ModelForm):

    class Meta:
        model = Job
        exclude = ('slug', 'status', 'created_at', 'udpated_at',)
