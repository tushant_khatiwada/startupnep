from django.apps import AppConfig


class PitchIdeaConfig(AppConfig):
    name = 'pitch_idea'
